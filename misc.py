import scipy.stats as stats
import os
import pandas as pd
import numpy as np
import math
import random

def n_nucs(n):
    '''creates all possible lists of nucleotides n long'''
    nucs = ['A','C','G','T']
    new_list = nucs.copy()

    for i in range(n-1):
        new_list =[a+b for a in new_list for b in nucs]
    return new_list  


comp={'G':'C','C':'G','A':'T','T':'A'}
rev_comp = lambda string :''.join([comp.get(i,'N') for i in string[::-1]])

def add_dics(a,b):
    return dict(zip(list(a.keys())+list(b.keys()),list(a.values())+list(b.values())))

def add_multi_dics(dic_list):
    a = dic_list[0]
    for i in dic_list[1:]:
        a = add_dics(a,i)
    return a

def translate(string):
    dic ={'AAA': 'K', 'AAC': 'N', 'AAG': 'K', 'AAT': 'N', 'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T', 'AGA': 'R',
    'AGC': 'S', 'AGG': 'R', 'AGT': 'S', 'ATA': 'I', 'ATC': 'I', 'ATG': 'M', 'ATT': 'I', 'CAA': 'Q', 'CAC': 'H', 'CAG': 'Q',
    'CAT': 'H', 'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P', 'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R', 'CTA': 'L',
    'CTC': 'L', 'CTG': 'L', 'CTT': 'L', 'GAA': 'E', 'GAC': 'D', 'GAG': 'E', 'GAT': 'D', 'GCA': 'A', 'GCC': 'A', 'GCG': 'A',
    'GCT': 'A', 'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G', 'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V', 'TAA': '*',
    'TAC': 'Y', 'TAG': '*', 'TAT': 'Y', 'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S', 'TGA': '*', 'TGC': 'C', 'TGG': 'W',
    'TGT': 'C', 'TTA': 'L', 'TTC': 'F', 'TTG': 'L', 'TTT': 'F'}
    a=string.upper()
    a.replace('U','T')
    a.replace('\n','')
    
    return ''.join([dic.get(a[3*i:3*(i+1)],'X') for i in range(len(a)//3)])

def filtr(df,dic):
    '''Takes two arguments - df and dic. df is a dataframe, dic is a dictionary of functions, 
    returns df filtered by the columns and conditions in dic'''
    df0 = df.copy()
    for d in dic:
        df0 = df0.loc[df0[d].map(dic[d])]
    return df0

def get_chisquare(myarray, columns = [], index = []):
    
    '''Given an array of 4 numbers, reshapes them as 2*2 matrix, and then calculates the expected matrix. Uses this 
    info to calculate a chisquare statistic
    
    Input -1*4 np.array, optional column and index lists
    Output - dictionary of the observed , expected and chi'''
    
    observed0 = pd.DataFrame(myarray.reshape(2,2))
    if columns != []:
        observed0.columns = columns
    if index !=[]:
        observed0.index = index

    observed = observed0.copy()
    observed.loc['t'] = observed.sum()
    observed['t'] = observed.T.sum()

    expected = pd.DataFrame([[i*j for i in observed.loc['t']] for j in observed['t']] )/observed['t']['t']
    expected0 = expected[[0,1]][:2]
    expected.columns = observed.columns
    expected.index = observed.index

    chi = stats.chisquare(observed0.as_matrix(), expected0.as_matrix(),axis = None)
    return {'chi':chi,'observed':observed,'expected': expected}


def find(string,dire):
    '''find out which notebooks mention the string in a given directory
    Input - string, directory
    Output - list of relevant notebooks'''
    notebooks = [i for i in os.listdir(dire) if i[-5:]=='ipynb']
    check = []
    for n in notebooks:
        with open(dire+n,'r') as f:
            if string in f.read():
                check.append(n)
    return check

def rep_n(string,n):
    '''Calculates how many times the first n characters are repeated continuously in the string.
    Input - string, n 
    Output - number of repetitions'''
    s = string+'*' 
    return next(( i for i in range(len(s)) if s[n*i:n*(i+1)]!=s[:n]))


log = np.vectorize(lambda x: math.log(x+1))

def n_examples_m(n,m_list):
    '''Given a list m long return n examples chosen at random'''
    new = []
    copy = m_list.copy()
    m = len(m_list)
    for i in range(n):
        new.append(copy.pop(random.randint(0,m-1)))
        m-=1
    return new
    
def count_values(df,column1,column2):
    '''Returns a dataframe with index=column1, columns=column2
  Entry i,j is the number of records in df with column1=i, column2 = j '''
    index = list(set(df[column2]))
    vc = pd.Series(df.groupby(by = column1).groups).map(
      lambda x: df.reindex(x)[column2]\
        .value_counts().reindex(index).fillna(0).astype(int))
    return pd.DataFrame(list(vc),index = vc.index)

def recursive(n,mylist=[]):
    if n==0:
        return mylist
    elif len(mylist)==0:
        return recursive(n-1,['A','C','G','T'])
    else:
        return recursive(n-1,[i+j for i in mylist for j in 'ACGT'])

def splitrown(row,n): 
    return [row[i*n:(i+1)*n] for i in range(len(row)//n)]
