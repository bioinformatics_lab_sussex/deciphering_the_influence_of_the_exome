import pandas as pd
import json
import numpy as np
import os


changes  = ['{}>{}'.format(i,j) for i in 'ACGT' for j in 'ACGT' if i!=j]

class Data:

    path = input('''path to data. This will normally be left blank but
                may be changed if you've moved the files to a separate data
                directory 
                 ''')
    sixes = [i+j+k+l+m+n for i in 'ACGT'  for j in 'ACGT'  for k in 'ACGT'  for l in 'ACGT' for m in 'ACGT' for n in 'ACGT'] 
    triplets = [i+j+k for i in 'ACGT' for j in 'ACGT' for k in 'ACGT']
    
    def add(name,method = '',nickname=''):

        method,name = getattr(Data,method)(name)
        if nickname!='':
            name = nickname
        if '_'+name not in Data.__dict__:
            if 'get_'+name not in Data.__dict__:
                setattr(Data,'get_'+name,method)
            setattr(Data,name,getattr(Data,'run')(name))
            
    def run(name):
        
        def deliver():
            if '_'+name not in Data.__dict__:
                setattr(Data,'_'+name,getattr(Data,'get_'+name)())
                print('loaded {}'.format(name))
            return getattr(Data,'_'+name)
        
        return deliver
            
    def auto(name):
        
        if name[-4:]=='json':
            def method():
                with open(os.path.join(Data.path,name)) as g:
                    f=json.load(g)
                return f
            
        elif name[-3:]=='csv':
            def method():
                return pd.read_csv(os.path.join(Data.path,name),index_col = 0,header = 0)

        elif name[-3:]=='tsv' or name[-3:]=='tab'  :
            def method():
                return pd.read_csv(os.path.join(Data.path,name),index_col = 0,header = 0, sep = '\t')

        elif (name[-3:]=='txt') or (name[-3:]=='seq'):
            def method():
                with open(os.path.join(Data.path,name)) as g:
                    f = g.read()
                return f
        
        n = name.split('/')[-1].split('.')[0]
        return method,n

    def series(name):
        def method():
            return pd.read_csv(os.path.join(Data.path,name),index_col=0,header = None,squeeze = True)
        n = name.split('/')[-1].split('.')[0]
        return method,n

    def series_of_lists(name):
        def method():
            return pd.read_csv(os.path.join(Data.path,name),index_col=0,header = None,squeeze = True).map(eval)
        n = name.split('/')[-1].split('.')[0]
        return method,n

        
    def delete(name):
        try:
            delattr(Data,'_'+name)
        except AttributeError:
            print('{} not found'.format(name))

    def mut_loader():
        p = os.path.join(path,'CosmicGenomeScreensMutantExport.tsv')
        return    pd.read_csv(p,index_col = None, sep = '\t')

datasets = [('signatures_probabilities.csv','auto','sig_probs'), #database of probabilities of each quad in each signature
            ('complete_insertions.csv','auto',''), #complete database of insertions
            ('sigs_pres.csv','auto',''),# signatures present for each cancer
            ('samples_for_each_primary_site.csv','series_of_lists','samples'), #complete database of insertions
            ('complete_deletions.csv','auto',''), #complete database of deletions
            ('sub_chromo.csv','auto',''), #
            ('ins_extra.csv','auto',''), #insertions together with extra info
            ('del_extra.csv','auto',''), #deletions together with extra info
            ('converter.csv','auto',''), #multiconverter from ensembl 38
            ('longest.csv','auto',''), #database of longest ensts for each gene
            ('muts.csv','auto',''), #database of mutations
            ('corr_del.csv','auto',''), #database of mutations
            ('corr_ins.csv','auto',''), #database of mutations
            ('mutation_breakdown.csv','series_of_lists',''),#lists of indices for the most common mutation types
            ('sixes_distribution.csv','series',''),

#distribution of sixes in the exome - note that each ENST is read in the order of encoding
            ('triplets_distribution.csv','series',''),
#distribution of triplets in the exome - note that each ENST is read in the order of encoding 
            ('logged_normalised_distribution_for_missense_substitutions.csv','series','missense_lnd'),
#logged fold enrichment for missense substitutions. Note that the sextuplets are reversed 
            ('logged_normalised_distribution_for_silent_substitutions.csv','series','silent_lnd'),
#logged fold enrichment for silent substitutions. Note that the sextuplets are reversed 
            ('logged_normalised_distribution_for_nonsense_substitutions.csv','series','nonsense_lnd'),
#logged fold enrichment for nonsense substitutions. Note that the sextuplets are reversed 
            ('logged_normal_deletion_frameshift.csv','series','del_frameshift_lnd'),
#logged fold enrichment for deletion frameshift. Note that the sextuplets are reversed 
            ('logged_normal_deletion_inframe.csv','series','del_inframe_lnd'),
#logged fold enrichment for deletion inframe. Note that the sextuplets are reversed 
            ('logged_normal_insertion_frameshift.csv','series','ins_frameshift_lnd'),
#logged fold enrichment for insertion frameshift. Note that the sextuplets are reversed 
            ('logged_normal_insertion_inframe.csv','series','ins_inframe_lnd'),
#logged fold enrichment for insertion inframe. Note that the sextuplets are reversed 
            ('log_values_for_subs_and_indels.csv','auto','lnds'),
#logged fold enrichment for subs and indels. Note that the sextuplets are reversed
            ('sub_lnds.csv','auto',''),
#logged fold enrichment for individual subs. Note that the sextuplets are reversed.
            ('log_ins_lengths.csv','series',''),
#logged frequency for insertions of length 1 to 60
			('log_del_lengths.csv','series',''),
#logged frequency for deletions of length 1 to 60

            ('NW_analysis_perc_del.csv','auto','nw_perc_del'),
#percentage of deletions found with perfect or good repeats next to them for lengths 1 to 9
            ('NW_analysis_perc_ins.csv','auto','nw_perc_ins'),
#percentage of insertions found with perfect or good repeats next to them for lengths 1 to 9
            ('NW_analysis_del.csv','auto','nw_del'),
#number of deletions found with perfect or good repeats next to them for lengths 1 to 9
            ('NW_analysis_ins.csv','auto','nw_ins'),
#number of insertions found with perfect or good repeats next to them for lengths 1 to 9
            ('coding_mutations_with_quads_from_vcf.csv','auto','quads'),
#quads for each of the mutation ids
            ('fingerprints.csv','auto',''),
#the mutational fingerprint for most of the samples in the cosmic database.
            ('non_coding_indels.csv','auto',''),
#database of all the non protein coding indels
            ('numbers_of_del_repeats_in_non-coding_region.csv','auto','nc_del_rep_nums'),
#numbers of deletions in non protein coding region that do and do not have repeats
            ('numbers_of_ins_repeats_in_non-coding_region.csv','auto','nc_ins_rep_nums'),
#numbers of insertions in non protein coding region that do and do not have repeats
            ('ins_neg_sel.csv','auto',''), #numbers of insertions of di and trinucleotides in coding and non-coding regions
            ('del_neg_sel.csv','auto',''), #numbers of deletions of di and trinucleotides in coding and non-coding regions
            ('ins_heatmap.csv','auto',''),#data for the insertion heatmap non-coding
            ('del_heatmap.csv','auto',''),#data for the deletion heatmap non-coding
            ('distribution_ins.csv','auto',''),#data for the insertion heatmap coding
            ('distribution_dels.csv','auto',''),#data for the deletion heatmap coding
            ('lg_dist_dels','auto',''),#logged data for the deletion heatmap coding
            ('lg_dist_ins','auto',''),#logged data for the insertion heatmap coding
            
]

# now lets add the fold enrichments for specific substitutions of form G>T etc.
for change in changes:
    name = 'logged_normalised_distribution_for_{}_substitutions.csv'.format(change)
    method = 'series'
    nickname = '{}_lnd'.format(change).replace('>','_')
    datasets.append((name,method,nickname))


print('Data includes')
    
for name,method,nickname in datasets:
    Data.add(name,method,nickname)
    name = name.split('/')[-1].split('.')[0]
    if nickname =='':
        nickname = name
    print(nickname,', ', end = '')


def add_chromosomes(path_to_chromosomes):
	'''Add in all the chromosomes once the path is known'''
	for chromo_num in range(1,23):
		path = os.path.join(path_to_chromosomes,'Homo_sapiens.GRCh38.dna.chromosome.{}.seq'.format(chromo_num))
		nickname = 'chromo{}'.format(chromo_num)
		Data.add(path,method='auto',nickname = nickname)

